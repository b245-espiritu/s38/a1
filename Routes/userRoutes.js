const express = require('express')

const router = express.Router();

const userController = require('../Controllers/userController.js')


router.post('/register', userController.userRegistration)

router.post('/login', userController.userAuthentication)

router.post('/details', userController.getProfile)

module.exports = router;